<?php

// main config file
// set default timezone
date_default_timezone_set('Australia/Sydney');
locale_set_default(getenv('LOCALE') ? getenv('LOCALE') : 'en_AU');
defined('LOCALE') || define('LOCALE', getenv('LOCALE') ?  : 'en_AU');// (locale_get_default() ?  : 'en_AU'));

// define some constants
defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV') ?  : 'development');
defined('MODULE_PATH') || define('MODULE_PATH', getenv('MODULE_PATH') ? getenv('MODULE_PATH') : realpath( __DIR__ . '/../module'));
defined('VENDOR_PATH') || define('VENDOR_PATH', getenv('VENDOR_PATH') ? realpath(getenv('VENDOR_PATH')) : realpath(__DIR__ . '/../vendor'));

// what is the current site's directory path if exists
$siteHostname = array_key_exists('HTTP_HOST', $_SERVER) && isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'www.mypalpetapi.local';
defined('HOSTNAME') || define('HOSTNAME', $siteHostname);

// site path for uploads (private data [not public])
defined('SITE_PATH') || define('SITE_PATH', __DIR__ . '/../data' . DIRECTORY_SEPARATOR . HOSTNAME);

// site path for public data
defined('PUBLIC_PATH') || define('PUBLIC_PATH', __DIR__ . '/../public');

return [
    'database' => [
        'host' => 'localhost',
        'dbname' => 'ensemble',
        'user' => 'ensemble',
        'password' => 'Welcome@2017'
    ]
];