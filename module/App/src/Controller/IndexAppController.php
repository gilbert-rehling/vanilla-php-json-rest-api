<?php

namespace App\Controller;

class IndexAppController
{
    /**
     * @var $pdo \Slim\PDO\Database
     */
    private $pdo;

    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * IndexAppController constructor.
     *
     * @param null $config
     */
    public function __construct( $config = null )
    {
        if (isset($config)) {
            $this->config = $config;
            $this->getPdo();
        }
    }

    /**
     * Index action loads directory listing and form
     */
    public function indexAction()
    {
        if ($this->pdo) {
            $customers = $this->fetchCustomers();
            $products  = $this->fetchProducts();
            $orders    = $this->fetchOrders();

            include_once __DIR__ . '/../../view/index.phtml';
        }
    }

    /**
     *  Sets up the PDO interface
     */
    private function getPdo()
    {
        if ($this->config) {
            $dsn = 'mysql:host=' . $this->config['database']['host'] . ';dbname=' . $this->config['database']['dbname'] . ';charset=utf8';
            $usr = $this->config['database']['user'];
            $pwd = $this->config['database']['password'];
            $this->pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);
        }
    }

    /**
     * @return array
     */
    private function fetchCustomers()
    {
        // SELECT * FROM customers
        $selectStatement = $this->pdo->select()
            ->from('customers');

        $stmt = $selectStatement->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return array
     */
    private function fetchProducts()
    {
        // SELECT * FROM products
        $selectStatement = $this->pdo->select()
            ->from('products');

        $stmt = $selectStatement->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return array
     */
    private function fetchOrders()
    {
        // SELECT * FROM orders
        $selectStatement = $this->pdo->select()
            ->from('orders');
        $selectStatement->leftJoin('customers', 'orders.customer_id', '=', 'customers.customer_id');
        $selectStatement->leftJoin('products', 'orders.product_id', '=', 'products.product_id');

        $stmt = $selectStatement->execute();
        return $stmt->fetchAll();
    }

}
