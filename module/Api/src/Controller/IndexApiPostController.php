<?php

namespace Api\Controller;

class IndexApiPostController
{
    /**
     * @var $pdo \Slim\PDO\Database
     */
    private $pdo;

    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * IndexApiController constructor.
     *
     * @param $config
     */
    public function __construct( $config = null )
    {
        if (isset($config)) {
            $this->config = $config;
            $this->getPdo();
        }
    }

    /**
     * Index action saves a POST
     */
    public function indexAction()
    {
        if ($this->pdo) {
            $order = $_POST['order'];
            $order = json_decode($order);
            $array = explode("&", $order);
            $order = array();
            foreach ($array as $arr) {
                $split = explode("=", $arr);
                $order[] = array($split[0] => $split[1]);
            }
            if (count($order) >= 1) {
                // save data
                $this->processPost( $order );

            } else {
                // return a failure response
                $data = array("success" => 0, "post" => "failed");
                header('Content-Type: application/json');
                echo json_encode($data);
            }
        }
    }

    /**
     *  Sets up the PDO interface
     */
    private function getPdo()
    {
        if ($this->config) {
            $dsn = 'mysql:host=' . $this->config['database']['host'] . ';dbname=' . $this->config['database']['dbname'] . ';charset=utf8';
            $usr = $this->config['database']['user'];
            $pwd = $this->config['database']['password'];
            $this->pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);
        }
    }

    /**
     * Saves a POST to database
     *
     * @param $orders array
     */
    private function processPost( $orders )
    {
        // break up the order and build the customer and products arrays and get the quantity value
         // santizize the values along the way
        $quantity  = 0;
        $customers = array(); // this will contain customer_id values
        $products  = array(); // this will contain product_id values

        // iterate the numerically indexed orders
        foreach ($orders as $order) {
            foreach ($order as $key => $value) {
                // get the quantity - we only need it once
                if ($key == 'quantity' && $quantity == 0 && is_int($value)) {
                    $quantity = $this->sanatizeData($value);
                }
                // get the customers
                if ($key == 'customer_id') {
                    // on case this is an array
                    if (is_array($value)) {
                        foreach ($value as $val) {
                        // validate
                            if (!empty($val) && is_int($val)) {
                                $customers[] = $this->sanatizeData($val);
                            }
                        }

                    } else {
                        // validate
                        if (!empty($value) && is_int($value)) {
                            $customers[] = $this->sanatizeData($value);
                        }
                    }
                }
                // get the products
                if ($key == 'product_id') {
                    // on case this is an array
                    if (is_array($value)) {
                        foreach ($value as $val) {
                            // validate
                            if (!empty($val) && is_int($val)) {
                                $products[] = $this->sanatizeData($val);
                            }
                        }

                    } else {
                        // validate
                        if (!empty($value) && is_int($value)) {
                            $products[] = $this->sanatizeData($value);
                        }
                    }
                }
            }
        }

        // validation - confirm that there is at leas one customer and product reference
        if (count($customers) == 0 || count($products) == 0 ) {
            // return an error
            $data = array("success" => 0, "validation" => "failed");
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        // ensure that $quantity is set to a workable minimum
        if ($quantity == 0) { $quantity = 1; }

        // iterate the customers and save each product + quantity
        $inserted = array();
        foreach ($customers as $customer) {
            foreach ($products as $product) {
               $inserted[] = $this->savePost( $quantity, $customer, $product );
            }
        }

        header('Content-Type: application/json');
        if (count($inserted) >= 1) {
            $data = array("success" => 1, "inserted" => count($inserted));
            echo json_encode($data);

        } else {
            $data = array("success" => 0, "insert" => "failed");
            echo json_encode($data);
        }
    }

    /**
     * @param $quantity
     * @param $customer
     * @param $product
     * @return string
     */
    private function savePost( $quantity, $customer, $product )
    {
        $datetime = date("Y-m-d h:i:s", time());
        $insertStatement = $this->pdo->insert(array('order_id', 'customer_id', 'product_id', 'product_quantity', 'order_created'))
            ->into('orders')
            ->values(array('', $customer, $product, $quantity, $datetime));

        return $insertStatement->execute(false);
    }

    /**
     * Basic sanitizing method
     *
     * @param $input
     * @return array
     */
    private function sanatizeData( $input )
    {
        $val    = trim($input);
        $v      = filter_var($val, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $output = filter_var($v, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

        return $output;
    }
}