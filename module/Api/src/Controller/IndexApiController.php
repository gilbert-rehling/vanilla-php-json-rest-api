<?php

namespace Api\Controller;

class IndexApiController
{
    /**
     * @var $id integer
     */
    private $id = null;

    /**
     * @var $pdo \Slim\PDO\Database
     */
    private $pdo;

    /**
     * @var $config array (database configuration)
     */
    private $config;

    /**
     * IndexApiController constructor.
     *
     * @param null $config
     * * @param $id
     */
    public function __construct( $config = null, $id = null )
    {
        if ($id) {
            $this->id = (int) $id['id'];
        }
        if (isset($config)) {
            $this->config = $config;
            $this->getPdo();
        }
    }

    /**
     * Index action loads directory data
     */
    public function indexAction()
    {
        header('Content-Type: application/json');
        if ($this->pdo) {
            if ($this->id != null) {
                $orders = $this->fetchOrder( $this->id );

            } else {
                $orders = $this->fetchOrders( );
            }

            $data = array("success" => 1, "data" => $orders);
            echo json_encode($data);

        } else {
            $data = array("success" => 0, "database" => "pdo unavailable");
            echo json_encode($data);
        }
    }

    /**
     *  Sets up the PDO interface
     */
    private function getPdo()
    {
        if ($this->config) {
            $dsn = 'mysql:host=' . $this->config['database']['host'] . ';dbname=' . $this->config['database']['dbname'] . ';charset=utf8';
            $usr = $this->config['database']['user'];
            $pwd = $this->config['database']['password'];
            $this->pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);
        }
    }

    /**
     * @return array
     */
    private function fetchOrders()
    {
        // SELECT ALL FROM orders
        $selectStatement = $this->pdo->select()
            ->from('orders');
        $selectStatement->leftJoin('customers', 'orders.customer_id', '=', 'customers.customer_id');
        $selectStatement->leftJoin('products', 'orders.product_id', '=', 'products.product_id');

        $stmt = $selectStatement->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return array|bool
     * @param $id array
     */
    private function fetchOrder( $id )
    {
        if ($id) {
            // SELECT ONE FROM orders
            $selectStatement = $this->pdo->select()
                ->from('orders')->where('order_id', '=', $id);
            $selectStatement->leftJoin('customers', 'orders.customer_id', '=', 'customers.customer_id');
            $selectStatement->leftJoin('products', 'orders.product_id', '=', 'products.product_id');

            $stmt = $selectStatement->execute();
            return $stmt->fetch();
        }
        return false;
    }
}