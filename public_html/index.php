<?php

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' || php_sapi_name() === 'cli') {
    if (isset($_SERVER['REQUEST_URI'])) {
        $path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    } else {
        $path = realpath(__DIR__ . parse_url($_SERVER['PHP_SELF'], PHP_URL_PATH));
    }

    if (__FILE__ !== $path && is_file($path)) {
        return false;
    }
    unset($path);
}

// Composer autoloading
include __DIR__ . '/../vendor/autoload.php';

// Retrieve configuration
$appConfig = require __DIR__ . '/../config/application.config.php';

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

    $r->addRoute('GET', '/', 'IndexAppController');

    $r->addRoute('GET', '/app', 'IndexAppController');

    $r->addRoute('GET', '/orders', 'IndexApiController');

    // {id} must be an integer (\d+)
    $r->addRoute('GET', '/orders/{id:\d+}', 'IndexApiController');

    $r->addRoute('POST', '/orders', 'IndexApiPostController');

});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

// define the namespaces so we can load the remote classes
$nameSpaces = array(
    'IndexAppController' => 'App\\Controller',
    'IndexApiController' => 'Api\\Controller',
    'IndexApiPostController' => 'Api\\Controller'
);

// process the route info
$application = false;
$routeInfo   = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        header("Location: /404.html");
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        // ... call $handler with $vars

        $className = "\\$nameSpaces[$handler]\\$handler";

        //$application = new IndexAppController();
        $application = new $className( $appConfig, $vars );

        break;
}

$application->indexAction();