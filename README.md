# PHP JSON REST API README

RESTful JSON vanilla PHP web service project by Gilbert Rehling.

The URL's / and /app are the public front-end, listing all current orders and a form for adding new orders.

Full public access to all functions. (no authentication implemented)

## Basic Usage:

All Orders:

	* Url: /orders
	* Method: GET
	* Content type: JSON
	* Returns:
		- success: true
		- data:
			- customer_id
			- customer_name
			- customer_email
			- customer_phone
			- customer_created
			- product_id
			- product_name
			- product_description
			- product_created
			- order_id
			- order_quantity
			- order_created

Single Item:

	* Url: /orders/<order_id>
	* Method: GET
	* Content type: JSON
	* Returns:
		- success: true
		- data:
			- customer_id
			- customer_name
			- customer_email
			- customer_phone
			- customer_created
			- product_id
			- product_name
			- product_description
			- product_created
			- order_id
			- order_quantity
			- order_created
		
POST Order:

	* Url: /orders
	* Method: POST
	* Content type: JSON
	* Requires:
		- customer_id (can be array)
		- product_id  (can be array)
		- quantity
	* Returns:
		- success: true
		- count: number if items added


http://jsonapi.gilbert-rehling.com

Copyright (c) 2017 Gilbert Rehling (www.gilbert-rehling.com)
